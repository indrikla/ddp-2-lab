import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Java{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan jumlah baris: ");
        int baris = input.nextInt();

        System.out.print("Masukkan jumlah kolom: ");
        int kolom = input.nextInt();

        System.out.println("Input Password: ");
        int[][] arr = new int[baris][kolom];

        for(int i=0; i<baris; i++){
            for(int n=0; n<kolom; n++){
                arr[i][n]=input.nextInt();
            }
        }
        input.close();
        System.out.println("Output: ");

        for(int i = arr.length-1; i >= 0; i--) {
            int[] arr2 = arr[i];
            for(int j = arr2.length-1; j >= 0; j--) {
                System.out.print(arr2[j] + " ");
            }
        System.out.println();
        }
    }
}