package com.ddp2.lab10rabu.model;

import java.util.ArrayList;

public class Donatur {
    private static ArrayList<Donatur> daftarDonatur = new ArrayList<Donatur>();
    private String nama;
    private int jumlahDonasi;
    private String asalDaerah;

    public String getNama() {
        return this.nama;
    }

    public String getAsalDaerah() {
        return this.asalDaerah;
    }

    public int getJumlahDonasi() {
        return this.jumlahDonasi;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setAsalDaerah(String asalDaerah) {
        this.asalDaerah = asalDaerah;
    }

    public void setJumlahDonasi(int jumlahDonasi) {
        this.jumlahDonasi = jumlahDonasi;
    }

    // TODO: tambahkan getter dan setter lain jika diperlukan

    public static ArrayList<Donatur> getDaftarDonatur() {
        return daftarDonatur;
    }

    // fungsi untuk menambahkan donatur baru ke dalam daftar donatur
    public static void addDonatur(Donatur donatur) {
        daftarDonatur.add(donatur);
    }

    public static int getTotalDana() {
        // TODO: hitung total dana yang berhasil dikumpulkan dari seluruh donatur
        int total = 0;
        for (Donatur donatur : daftarDonatur){
            total += donatur.getJumlahDonasi();
        }
        return total;
    }

    public static int getTotalDonatur() {
        // TODO: hitung jumlah donatur yang telah menyumbang
        return daftarDonatur.size();
    }
}