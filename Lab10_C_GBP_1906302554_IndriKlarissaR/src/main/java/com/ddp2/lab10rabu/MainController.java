package com.ddp2.lab10rabu;

import com.ddp2.lab10rabu.model.Donatur;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class MainController {
    
    @GetMapping("/")
    public String index(Model model) {
        // TODO: pass variabel yang diperlukan untuk thymeleaf di URL /
        model.addAttribute("namaKamu", "Dek Depe");
        return "index";
    }

    @GetMapping("/form-donasi")
    public String formDonasi(Model model) {
        // TODO: pass variabel yang diperlukan untuk thymeleaf di URL /form-donasi
        model.addAttribute("formDonasi", new Donatur());
        return "form-donasi";
    }

    @PostMapping("/form-donasi")
    public String submitFormDonasi(@ModelAttribute Donatur donatur, Model model) {
        // TODO: tambahkan donatur yang diisi di form ke dalam daftar donatur
        Donatur.addDonatur(donatur);
        model.addAttribute("namaKamu", donatur.getNama());
        return "hasil-form-donasi";
    }

    // TODO: tambahkan mapping daftar donatur dan total donasi
    @GetMapping("/daftar-donatur")
    public String daftarDonatur(Model model) {
        model.addAttribute("donaturs", Donatur.getDaftarDonatur());
        return "daftar-donatur";
    }

    @GetMapping("/total-donasi")
    public String totalDonasi(Model model) {
        model.addAttribute("totalDonasi", Donatur.getTotalDana());
        model.addAttribute("totalDonatur", Donatur.getTotalDonatur());
        return "total-donasi";
    }

}