import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.io.FileInputStream;

public class CreatePlaylist extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        GridPane gp = new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setVgap(10);
        gp.setHgap(10);
        gp.setPadding(new Insets(25, 25, 25, 25));

        //TODO
        // membuat greetings text dan icon
        FileInputStream fis = new FileInputStream("popo.png");
        Image img = new Image(fis);
        ImageView iv = new ImageView(img);

        HBox greetings = new HBox();
        greetings.setAlignment(Pos.CENTER);
        greetings.setSpacing(10);
        greetings.getChildren().add(iv);

        Text hello = new Text("Hello, Dek Depe!");
        
        greetings.getChildren().add(hello);
        gp.add(iv, 0, 1);
        iv.setFitHeight(80);
        iv.setPreserveRatio(true);
        gp.add(hello, 1, 1);

        //TODO
        // membuat label dan text field playlist
        gp.add(new Label("Playlist name: "), 0, 2);
        TextField playlistNameField = new TextField();
        gp.add(playlistNameField, 1, 2);

        //TODO
        // membuat label dan text field lagu
        gp.add(new Label("Song title: "), 0, 3);
        TextField songNameField = new TextField();
        gp.add(songNameField, 1, 3);

        //TODO
        // membuat label dan choice box genre
        gp.add(new Label("Genre: "), 0, 4);
        String[] genre = {"Rock", "Jazz", "Pop"};
        ChoiceBox genreCB = new ChoiceBox();
        genreCB.getItems().add("Rock");
        genreCB.getItems().add("Jazz");
        genreCB.getItems().add("Pop");
        gp.add(genreCB, 1, 4);

        //TODO
        //membuat label dan text field artist
        gp.add(new Label("Artist name: "), 0, 5);
        TextField artistNameField = new TextField();
        gp.add(artistNameField, 1, 5);

        //TODO
        //membuat button
        Button btn = new Button("Add to Playlist");
        gp.add(btn, 0, 6);


        //membuat notifikasi
        btn.setOnAction(event -> {
            primaryStage.setTitle("Java Music | Succesfully added!");
            playlistNameField.clear();
            songNameField.clear();
            genreCB.getItems().clear();
            artistNameField.clear();
        });

        //TODO
        Scene createPlaylist = new Scene(gp, 350, 350);
        primaryStage.setTitle("Java Music | Create Playlist");
        primaryStage.setScene(createPlaylist);
        primaryStage.setResizable(false);
        primaryStage.show();


    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
