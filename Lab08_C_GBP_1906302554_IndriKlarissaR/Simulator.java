import java.util.ArrayList;
import java.util.List;
import java.io.File;  
import java.util.Scanner;
import java.io.FileNotFoundException;


public class Simulator {
    public static void main(String[] args) throws CivilWarOccurredException  {
        MiddleEarth middleEarth = new MiddleEarth();
       try {
            //TODO: Implement this try block
            List<String> creatureFile = readFile("Creature1.txt");
            for(int i = 0; i < creatureFile.size(); i++){
                String[] str = new String[3];
                if(creatureFile.get(i).length()>1){
                    str = creatureFile.get(i).split(" ");
                    int health = Integer.parseInt(str[2]);
                    middleEarth.addCreature(str[0], str[1], health);
                }   
            }
            System.out.println("THE WAR BEGINS!!");
        
            List<String> warFile = readFile("War1.txt");
            for(int i = 0; i < warFile.size(); i++){
                String[] strwar = new String[2];
                if(warFile.get(i).length()>1){
                    strwar = warFile.get(i).split(";");
                    middleEarth.doFight(strwar[0], strwar[1]);    
            }
        }
            
        }
        catch(CantCreateException e) {
            //TODO: Implement this exception block
            System.out.println("");
        }
        finally {
                //TODO: Implement this finally block
                System.out.println("MIDDLE EARTH DESTROYED");
        }
        
    }

    public static List<String> readFile(String filename) {
        //TODO: Implement this to read input file
        List<String> data = new ArrayList<String>();
        try {
            File myObj = new File(filename);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                data.add(myReader.nextLine());
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
        return data;
    }
}
