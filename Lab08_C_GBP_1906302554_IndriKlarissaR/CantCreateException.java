public class CantCreateException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public CantCreateException(String x) {
        super(x);
    }
}
