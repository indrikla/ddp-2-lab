import java.util.ArrayList;
import java.util.List;

public class MiddleEarth {
    //TODO: data field & Constructor
    private List<Creature> listCreature = new ArrayList<Creature>();

    public void addCreature(String type, String name, int health) throws CantCreateException {
        //TODO : Implement add creture to list

        if (type.equals("Elf")){
            if (health>3000){
                throw new CantCreateException("Maximum Health for" + name + "is 3000"); 
            }
            listCreature.add(new Elf(name, health));
        } else if (type.equals("Dwarf")){
            if (health>2200){
                throw new CantCreateException("Maximum Health for" + name + "is 2200"); 
            }
            listCreature.add(new Dwarf(name, health));
        } else {
            if (health>1500){
                throw new CantCreateException("Maximum Health for" + name + "is 1500"); 
            }
            listCreature.add(new Hobbit(name, health));
        }

    }

    public void doFight(String thisCreature, String thatCreature) throws CivilWarOccurredException {
        //TODO: implement doFight
        if (creatureFindByName(thisCreature).getType().equals(creatureFindByName(thatCreature).getType())){
            throw new CivilWarOccurredException("HEYOO CIVIL WAR OCCURRED"); 
        } else if(creatureFindByName(thatCreature).getHealth()<=0){
            System.out.printf("%s Death\n", creatureFindByName(thatCreature).getName());
        } else if(creatureFindByName(thisCreature).getHealth()<=0){
            System.out.printf("%s Death\n", creatureFindByName(thisCreature).getName());
        } else {
            while(creatureFindByName(thatCreature).getHealth()>0 ){
                creatureFindByName(thisCreature).doFight(creatureFindByName(thatCreature));
            }
        }
    }

    public Creature creatureFindByName(String name) {
        for (Creature creature : listCreature){
            if (creature.getName().equals(name)){
                return creature;
            }
        }
        return null;
    }

    public String getCreaturesInfo() {
        //TODO: implement get information of creature
        String result = "";
        for(Creature element: listCreature){
            result = result + element.getInfo() + "\n";
        }
        return result;
    }

    public List<Creature> getCreatureList() {
        return this.listCreature;
    }
}
