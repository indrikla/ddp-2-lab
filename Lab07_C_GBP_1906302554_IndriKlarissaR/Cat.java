// TODO: perbaiki class ini sesuai konsep OOP
public class Cat extends Pet {

	// TODO: tambahkan data field JIKA diperlukan
	static StatusBehavior idle = new Idle();

	// TODO: lengkapi constructor sesuai konsep OOP
	public Cat(String name, int age) {
		super(name, age, 100, idle);
		System.out.printf("%s has arrived!", toString());
		System.out.println();
	}

	// TODO: tambahkan method-method lain yang diperlukan
	
	public void makeSound(){
		System.out.printf("%s: Meowww", toString());
		System.out.println();
	}

	public void doPower(){
		System.out.printf("%s: Scratching . . .", toString());
		System.out.println();
	}
	public void play(){
		if (statusEquals("idle")) {
			System.out.printf("%s is playing . . .", toString());
			System.out.println();
			setHappiness(15);
		} else {
			System.out.printf("%s can't play right now!", toString());
			System.out.println();
		}
		
	}

	public String toString() {
		return String.format("[Cat - %d - %d] %s", getAge(), getHappiness(), getName());
	}
}
