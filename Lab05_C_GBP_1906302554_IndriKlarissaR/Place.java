import java.util.ArrayList;

/*
 * TO DO: 
 * Provide Constructor
 * Complete all TO DOs
 * Write setter and getter if needed
 */
class Place {

    private static ArrayList<Seller> sellers = new ArrayList<>();
    private String name;

    // TO DO
    public Place(String name){
        this.name = name;
    }
    
    public void add(Seller seller) {
        // TO DO
        sellers.add(seller);
    }

	public static Seller getSpecificSeller(String name) {
        // TO DO
        for(int i = 0; i < sellers.size(); i++) {
            if(sellers.get(i).getName().equals(name)){
                return sellers.get(i);
            }
        }
        return null;
	}

	public static int getTotalTarget() {
        // TO DO
        int totalTar = 0;
        for(int i = 0; i < sellers.size(); i++) {
            totalTar += sellers.get(i).getTarget();
        }
        return totalTar;
    }

    public static int getTotalProfit() {
        // TO DO
        int totalProf = 0;
        for(int i = 0; i < sellers.size(); i++) {
            totalProf += sellers.get(i).getProfit();
        }
        return totalProf;
    }

	public static int getNumOfSellers() {
        // TO DO
        return sellers.size();
    }

    /**
     * @return string representation of the Place
     */
    public String toString(){
        StringBuilder builder = new StringBuilder();
        for (Seller seller: sellers) {
            builder.append(seller.toString());
            builder.append("\n");
        }
        return builder.toString();
    }
}