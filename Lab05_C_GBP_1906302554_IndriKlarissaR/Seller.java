/*
 * TO DO: 
 * Provide Constructor
 * Write setter and getter if needed
 * Complete all TO DOs
 */
class Seller {

	private String name;
	private int price;
    private int target;
    private int transactions;

    // TO DO
    public Seller(String name,int price,int target){
        this.name = name;
        this.price = price;
        this.target = target;
        this.transactions = 0;
    }

    public void addTransactions(int number) {
        // TO DO
        transactions += number;
    }

    public int getProfit() {
        // TO DO
        return transactions*price;
    }
    public String getName() {
        return this.name;
    }
    public int getTarget() {
        return this.target;
    }
    public int getPrice() {
        return this.price;
    }
    public int getTransactions(){
        return this.transactions;
    }


    /**
     * @return string representation of the Seller
     */
    public String toString() {
        return String.format("%-10s  %7d  %10d", name, price, target);
    }

}